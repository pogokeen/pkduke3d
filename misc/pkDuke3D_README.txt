    $$$$$$$$               /KK      /DDDDDDD          /KK                /333333 /DDDDDDD       $$$$$$$$     
  $$ $$$$$$ $$            | KK     | DD__  DD        | KK               /33__  3| DD__  DD    $$ $$$$$$ $$   
 $    $$$$    $    /PPPPPP| KK   /K| DD  \ DD/UU   /U| KK   /KK /EEEEEE|__/  \ 3| DD  \ DD   $    $$$$    $  
$      $$      $  /PP__  P| KK  /KK| DD  | D| UU  | U| KK  /KK//EE__  EE  /33333| DD  | DD  $      $$      $ 
$$$$$$ $$ $$$$$$ | PP  \ P| KKKKKK/| DD  | D| UU  | U| KKKKKK/| EEEEEEEE |___  3| DD  | DD  $$$$$$ $$ $$$$$$ 
 $$$$$    $$$$$  | PP  | P| KK_  KK| DD  | D| UU  | U| KK_  KK| EE_____//33  \ 3| DD  | DD   $$$$$    $$$$$  
  $$$      $$$   | PPPPPPP| KK \  K| DDDDDDD|  UUUUUU| KK \  K|  EEEEEE|  333333| DDDDDDD/    $$$      $$$   
    $$$$$$$$     | PP____/|__/  \__|_______/ \______/|__/  \__/\_______/\______/|_______/       $$$$$$$$     
                 | PP                                                                                        
                 | PP                                                     (Version 1.3)                      
                 |__/                                                                                        


  Thanks for downloading and installing pkDuke3D! :)
  
  This major release (1.3) allows pkDuke3D to be set up & run from any Duke Nukem 3D: Atomic Edition 1.5 compatible .GRP (including World Tour).
  This release does not include many additional features over 1.2, but should hopefully make speedrunning the game more accessible.
  
  Fixes and improvements include:
   +Re-enabled MIDI audio playback when the pre-rendered Megaton soundtrack is not available & fixed issues with MIDI playback and volume adjustment
   +pkDuke3D now provides its own menus and UI resources for when the Megaton menus & UI resources are not available
   +pkDuke3D no longer provides the "Help" menu option instead of the "Community Hub" option in order to avoid some bugs with the
     libRocket menus navigation
   +In addition to supporting data files from other versions of Duke Nukem 3D,
     pkDuke3D verifies that the .GRP data files being used with it are equivalent/compatible versions.
     This change is meant to help make sure that new players are using the right data files with the
     game that are compatible with the speedrun leaderboards.
   +options for playing NAM have been removed, as those were carry overs from jfDuke3D.
     (Megaton & pkDuke3D don't match any official release's behaviour for NAM, so it's not a useful option for speedrunning)
  
  A full changelog can be seen in the commit history on BitBucket.
  
  Please note that the installation & upgrade instructions have changed for this release.
  It is recommended that you read them below before installing this version.
  
  If you are recording a demo, please remember to be cautious to also record a video for the sake of safety.
  
  Special thanks to everyone in the Build speedrunning community for being so patient while I had to put pkDuke3D updates on hiatus,
   and additional thanks to everyone who tested this release!
  Good luck & enjoy your speedruns!
   ~pogokeen



pkDuke3D is a source port based on Duke Nukem 3D: Megaton Edition.
pkDuke3D adds features useful to speedrunners while avoiding any modification to gameplay.


Features:

+ Supports IL & continuous demo (.DMO) recording and playback (/r and /rc on the command-line, respectfully)
+ Provides the option to disable demo cameras (/k on the command-line)
+ When in demo (.DMO) playback mode (/dFILE on the command-line), proper level music is played rather than the main menu theme.
  Additionally, demo statistics are presented at the end of demo playback!
+ Adds a variety of timers -- track your runs with the classic Duke3D timer or the new accurate Game-Time & Real-Time timers
+ Tracks the speedrun categories your run qualifies for (including Any%, 100S, 100%, Max%)
+ Built on and true to version 1.3.2 of Megaton Edition, one of the nicest versions of Megaton Edition for speedrunning
+ Can be installed from any Duke Nukem 3D Atomic Edition 1.5 compatible .GRP (including World Tour)


Installation for Players without the Megaton Edition:

1. Extract pkDuke3D.zip in a new folder where you would like the game to be installed (or over an existing pkDuke3D install)
2. If you are not upgrading an existing pkDuke3D install,
    copy DUKE3D.GRP into your pkDuke3D folder.
   This file must come from an existing Duke Nukem 3D Atomic Edition 1.5 compatible install (including World Tour).
    (for World Tour, you can typically find this file in
     "C:\Program Files (x86)\Steam\steamapps\common\Duke Nukem 3D Twentieth Anniversary World Tour")
3. Once the DUKE3D.GRP file is copied to the right place, you can now run pkDuke3D.exe
4. (Optional) If you would like to be able to speedrun the addon expansions (Duke It Out in D.C., Caribbean, or Nuclear Winter),
    you will need to copy the add-on game data files into the following places:
 Duke It Out in DC: "addons\dc\dukedc.grp"
         Caribbean: "addons\vacation\vacation.grp"
    Nuclear Winter: "addons\nw\nwinter.grp"


Installation for Players with the Megaton Edition:

1. Copy the "gameroot" folder from your Megaton Edition directory
    (typically "C:\Program Files (x86)\Steam\steamapps\common\Duke Nukem 3D\gameroot" for a Steam install)
    to the directory where you want pkDuke3D installed.
2. Rename this copied "gameroot" folder to "pkDuke3D".
2. Extract pkDuke3D.zip within that folder, replacing any existing files.

It is highly recommended to extract into a copy, rather than in your normal Megaton install directory, as the two versions WILL CLASH.


Upgrade Instructions for Players coming from pkDuke3D 1.2:

1. If you have a separate "gameroot" and "bin" folders,
    please move all of the files and folders from within both folders into your higher level "pkDuke3D" folder.
   After this operation, folder like "addons" and "data" should now be direct subfolders within your "pkDuke3D" folder.
2. Extract pkDuke3D.zip within that folder, replacing any existing files.


How To Use:

If you've set up your install as above, simply start pkDuke3D.exe to play.

Within the gameroot command-prompt, type:
pkDuke3D.exe /S1 /V1 /L1 /rc
to start recording a continuous demo on Skill 1 (/S1), Episode 1 (/V1), Level 1 (/L1).
This demo will continue to record until you exit the game.
Note that this will overwrite DEMO1.DMO with your recorded demo.

To record an IL demo:
pkDuke3D.exe /S1 /V1 /L1 /r
will start recording an IL demo on Skill 1 (/S1), Episode 1 (/V1), Level 1 (/L1).
This demo will continue to record until you die, exit the level, or quit the game.
Note that this will overwrite DEMO1.DMO with your recorded demo.

To playback a demo:
pkDuke3D.exe /dDEMO1.DMO
will playback DEMO1.DMO in demo playback mode.
Note that playing back a demo with any other name will replace DEMO1.DMO with that demo.

Add the /k switch to disable demo cameras in both gameplay and demo playback!

Further, note that any command-line switches can also be provided in the target of a Windows/Steam Shortcut!


Command Line Help (A List of The Command-Line Switches For pkDuke3D):

?, /?          This help message
/l##           Level (1-11)
/v#            Volume (1-4)
/s#            Skill (1-4)
/r             Record Individual Level Demo
/rc            Record Continuous Demo
/dFILE         Start to play demo FILE
/k             Disable demo cameras
/m             No monsters
/ns            No sound
/nm            No music
/t#            Respawn, 1 = Monsters, 2 = Items, 3 = Inventory, x = All
/c#            MP mode, 1 = DukeMatch(spawn), 2 = Coop, 3 = Dukematch(no spawn)
/q#            Fake multiplayer (2-8 players)
/a             Use player AI (fake multiplayer only)
/i#            Network mode (1/0) (multiplayer only) (default == 1)
/f#            Send fewer packets (1, 2, 4) (multiplayer only)
/gFILE         Use multiple group files
/jDIRECTORY    Add a directory to the file path stack
/hFILE         Use FILE instead of DUKE3D.DEF
/xFILE         Compile FILE (default GAME.CON)
/u#########    User's favorite weapon order (default: 3425689071)
/#             Load and run a game (slot 0-9)
-map FILE      Use a map FILE
-name NAME     Foward NAME
-net           Net mode game
-setup         Displays the configuration dialogue box
-addon #       Set which add-on to play (0 = Atomic [Default], 1 = D.C., 2 = Nuclear Winter, 3 = Caribbean)
-noanim        Disable animations
-delete-saves  Delete all saves from cloud and locally


Legal:

"Build Engine & Tools" Copyright (c) 1993-1997 Ken Silverman
Ken Silverman's official web site: "http://www.advsys.net/ken"
See the included license file "BUILDLIC.TXT" for license info.

pkDuke3D Modifications are Copyright (C) 2014-2021 Alexander "pogokeen" Dawson

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
